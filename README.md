Ansible lookup plugins
======================

[Lookup plugins](http://docs.ansible.com/ansible/2.4/dev_guide/developing_plugins.html#lookup-plugins)
are used to pull in data from external data stores.

This repository contains the following custom plugins:

  - csentry: retrieve data from [CSEntry](http://ics-infrastructure.pages.esss.lu.se/csentry/index.html)


License
-------

BSD 2-clause
